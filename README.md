# LoRa Contact

## Overview

### Owners & Administrators

| Title       | Details                                                                                          |
|-------------|--------------------------------------------------------------------------------------------------|
| **E-group** | [covid-distancing-admin](https://groups-portal.web.cern.ch/group/covid-distancing-admin/details) |
| **People**  | [Alessandro Zimmaro](https://phonebook.cern.ch/search?q=Alessandro+Zimmaro)                      |

### Kafka Topics

| Environment    | Topic Name                                                                                                |
|----------------|-----------------------------------------------------------------------------------------------------------|
| **Production** | [lora-contact](https://nile-kafka-ui.app.cern.ch/ui/clusters/gp3/all-topics/lora-contact)                 |
| **Production** | [lora-contact-decoded](https://nile-kafka-ui.app.cern.ch/ui/clusters/gp3/all-topics/lora-contact-decoded) |
| **QA**         | [lora-contact](https://nile-kafka-ui.app.cern.ch/ui/clusters/qa3/all-topics/lora-contact)                 |
| **QA**         | [lora-contact-decoded](https://nile-kafka-ui.app.cern.ch/ui/clusters/qa3/all-topics/lora-contact-decoded) |

### Configuration

| Title                        | Details                                                                                          |
|------------------------------|--------------------------------------------------------------------------------------------------|
| **Application Name**         | covid-distancing                                                                                 |
| **Configuration Repository** | [app-configs/lora-contact](https://gitlab.cern.ch/nile/streams/app-configs/lora-contact) |
